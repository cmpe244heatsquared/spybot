#include "proximity.hpp"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "printf_lib.h"

#include "gpio.hpp"
#include "io.hpp"
#include "vl53l0x_i2c_platform.h"
#include "vl53l0x_api.h"

#define SENSOR_BASE_ADDR 0x52
#define I2C_FREQ_KHZ 100U

typedef struct {
    VL53L0X_Dev_t sensor_handles[PROXIMITY_LAST_INDEX];
    uint16_t sensor_distances[PROXIMITY_LAST_INDEX];  // mm
    bool errors[PROXIMITY_LAST_INDEX];
    bool initialized;
} ProximityVars_S;

static ProximityVars_S prox_vars;

static bool initialize_sensors(void);

uint8_t vhv_settings = 0U;
uint8_t phase_cal = 0U;
uint32_t ref_spad_count = 0U;
uint8_t is_aperture_spads = 0U;


void PROXIMITY_init(void)
{
    VL53L0X_comms_initialise(I2C_FREQ_KHZ);

    for (uint8_t index=0U; index < PROXIMITY_LAST_INDEX; index++) {
        //memset(&prox_vars.sensor_handles[index], 0U, sizeof(VL53L0X_Dev_t));
        prox_vars.sensor_handles[index].I2cDevAddr = SENSOR_BASE_ADDR + index;
        prox_vars.sensor_distances[index] = 0U;
        prox_vars.errors[index] = false;
    }
    prox_vars.initialized = false;
}


void PROXIMITY_task(void *params)
{
    GPIO shutdown_gpio(P0_29);

    while (true) {
        if (!prox_vars.initialized) {
            shutdown_gpio.setAsOutput();
            shutdown_gpio.setLow();
            vTaskDelay(100U);
            shutdown_gpio.setHigh();
            prox_vars.initialized = initialize_sensors();
        }

        static uint8_t calibration_counter = 0U;
        if (calibration_counter++ >= 25U*4U*10U) {
            calibration_counter = 0U;
            for (uint8_t index=0U; index < PROXIMITY_LAST_INDEX; index++) {
                VL53L0X_Dev_t *sensor_handle = &prox_vars.sensor_handles[index];
                VL53L0X_Error error = VL53L0X_ERROR_NONE;
                if (calibration_counter >= 10U) {
                    error = VL53L0X_PerformRefCalibration(sensor_handle, &vhv_settings, &phase_cal);
                    if (error) {
                        prox_vars.errors[index] = true;
                        printf("FAILED TO PERFORM CALIBRATION!\n");
                    }   
                }
            }   
        }

        for (uint8_t index=0U; index < PROXIMITY_LAST_INDEX; index++) {
            VL53L0X_Error error = VL53L0X_ERROR_NONE;
            VL53L0X_Dev_t *sensor_handle = &prox_vars.sensor_handles[index];
            error = VL53L0X_PerformSingleMeasurement(sensor_handle);
            if (!error) {
                VL53L0X_RangingMeasurementData_t measurement_data = {0U};
                error = VL53L0X_GetRangingMeasurementData(sensor_handle, &measurement_data);
                if (!error) {
                    //printf("%d / [mm]; RangeStatus: %d\n", measurement_data.RangeMilliMeter, measurement_data.RangeStatus);
                    if (measurement_data.RangeStatus) {
                        char s[128U];
                        (void)VL53L0X_GetRangeStatusString(measurement_data.RangeStatus, s);
                        printf("%s\n", s);
                    }
                }

                if (!error) {
                    prox_vars.sensor_distances[index] = measurement_data.RangeMilliMeter;
                    if (measurement_data.RangeStatus == 0) {
                        prox_vars.errors[index] = false;
                    }
                    else {
                        prox_vars.errors[index] = true;
                    }
                }
            }
        }

        if ((PROXIMITY_is_initialized()) && (!PROXIMITY_is_error())) {
            LE.toggle(1);
        }
        else {
            LE.on(1);
        }
        vTaskDelay(25U);
    }
}


uint16_t PROXIMITY_distance(PROXIMITY_Sensor_E index)
{
    return prox_vars.sensor_distances[index];
}


bool PROXIMITY_is_error(void)
{
    bool error = false;
    for (uint8_t index=0U; index < PROXIMITY_LAST_INDEX; index++) {
        if (prox_vars.errors[index]) {
            error = true;
        }
    }
    return error;
}


bool PROXIMITY_is_initialized(void)
{
    return prox_vars.initialized;
}


static bool initialize_sensors(void)
{
    bool initialized = false;
    for (uint8_t index=0U; index < PROXIMITY_LAST_INDEX; index++) {
        VL53L0X_Error error = VL53L0X_ERROR_NONE;
        VL53L0X_Dev_t *sensor_handle = &prox_vars.sensor_handles[index];

        error = VL53L0X_DataInit(sensor_handle);
        if (error) {
            printf("FAILED TO INITIALIZE DATA!\n");
            return initialized;
        }

        error = VL53L0X_StaticInit(sensor_handle);
        if (error) {
            printf("FAILED TO INITIALIZE STATIC!\n");
            return initialized;
        }

        error = VL53L0X_PerformRefCalibration(sensor_handle, &vhv_settings, &phase_cal);
        if (error) {
            printf("FAILED TO PERFORM CALIBRATION!\n");
            return initialized;
        }

        error = VL53L0X_PerformRefSpadManagement(sensor_handle, &ref_spad_count, &is_aperture_spads);
        if (error) {
            printf("FAILED TO PERFORM SPAD MANAGEMENT!\n");
            return initialized;
        }
    }
    initialized = true;
    return initialized;
}
