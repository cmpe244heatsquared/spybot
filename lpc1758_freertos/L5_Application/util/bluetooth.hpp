
#include "stdio.h"
#include "printf_lib.h"
#include "uart2.hpp"
#include "string.h"

#define NEUTRAL 			15
#define WHEEL_STRAIGHT		15
#define CAMERA_STRAIGHT 	15


class BluetoothCar {

public:

	BluetoothCar()
	{
		motorPercentage = NEUTRAL;
		carServoPercentage = WHEEL_STRAIGHT;
		cameraServoVerticalPercentage = CAMERA_STRAIGHT;
		cameraServoHorizontalPercentage = CAMERA_STRAIGHT;
		steerPosition = false;
		motorPosition = false;
		cameraSteerHorizontal = false;
		cameraSteerVertical = false;
		urlReceived = false;
	}

	static BluetoothCar * getInstance(void)
	{
		static BluetoothCar *instance = NULL;
		if (instance == NULL) {
			instance = new BluetoothCar();
		}
		return instance;
	}

	void setMotorPercentage(float val)
	{
		motorPercentage = val;
	}

	void setCarServoPercentage(float val)
	{
		carServoPercentage = val;
	}

	void setCameraServoVerticalPercentage(float val)
	{
		cameraServoVerticalPercentage = val;
	}

	void setCameraServoHorizontalPercentage(float val)
	{
		cameraServoHorizontalPercentage = val;
	}

	void setSteerPosition(char *position)
	{
		if (0 == strcmp(position, "left"))
		{
			steerPosition = false;
		}
		else if(0 == strcmp(position, "right"))
		{
			steerPosition = true;
		}
	}

	void setMotorPosition(char *position)
	{
		if (0 == strcmp(position, "forward"))
		{
			motorPosition = false;
		}
		else if(0 == strcmp(position, "backward"))
		{
			motorPosition = true;
		}
	}

	void setCameraHorizontalPosition(char *position)
	{
		if (0 == strcmp(position, "left"))
		{
			cameraSteerHorizontal = false;
		}
		else if(0 == strcmp(position, "right"))
		{
			cameraSteerHorizontal = true;
		}
	}

	void setCameraVerticalPosition(char *position)
	{
		if (0 == strcmp(position, "up"))
		{
			cameraSteerVertical = false;
		}
		else if(0 == strcmp(position, "down"))
		{
			cameraSteerVertical = true;
		}
	}

	
	float getMotorPercentage(void) 
	{
		return motorPercentage;
	}

	float getCarServoPercentage(void)
	{
		return carServoPercentage;
	}

	float getCameraServoVerticalPercentage(void)
	{
		return cameraServoVerticalPercentage;
	}

	float getCameraServoHorizontalPercentage(void)
	{
		return cameraServoHorizontalPercentage;
	}

	/*
		@return true if position is right, false if position is left
	*/
	bool getSteerPosition(void)
	{
		return steerPosition;
	}


	/*
		@return true if backwards, false if forward
	*/
	bool getMotorPosition(void)
	{
		return motorPosition;
	}

	/*
		@return true if position is right, false if position is left
	*/
	bool getCameraHorizontalPosition(void)
	{
		return cameraSteerHorizontal;
	}


	/*
		@return true if down, false if up
	*/
	bool getCameraVerticalPosition(void)
	{
		return cameraSteerVertical;
	}


	void sendUrlToCar(const char *url)
	{
		static char messageToCar[100] = {'\0'};

		sprintf(messageToCar, "address ip %s", url);
		// u0_dbg_printf("%s\r\n", messageToCar);

		sendMessageToCar(messageToCar);
	}

	void sendDistanceToCar(const char *data)
	{
		char messageToCar[100] = {'\0'};

		sprintf(messageToCar, "sensor distance %s", data);

		sendMessageToCar(messageToCar);
	}

	void setUrlReceived(bool received) 
	{
		urlReceived = received;
	}

	bool getUrlReceived(void)
	{
		return urlReceived;
	}


private:
	float motorPercentage;
	float carServoPercentage;
	float cameraServoVerticalPercentage;
	float cameraServoHorizontalPercentage;
	bool steerPosition;
	bool motorPosition;
	bool cameraSteerHorizontal;
	bool cameraSteerVertical;

	bool urlReceived;

	void sendMessageToCar(char *msg)
	{
		// u0_dbg_printf("%s\r\n", msg);
		Uart2::getInstance().putline(msg);
	}


};
