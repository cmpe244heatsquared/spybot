#include "LPC17xx.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "bluetooth_app.hpp"
#include "io.hpp"
#include "printf_lib.h"
#include "bluetooth.hpp"

/*
	THIS IS ONLY A TEST
*/


vBluetooth::vBluetooth(uint8_t priority) : scheduler_task("Bluetooth", 512 * 4, priority)
{


}

bool vBluetooth::run(void *p)
{
    // float motor = BluetoothCar::getInstance()->getMotorPercentage();
    // float carServo = BluetoothCar::getInstance()->getCarServoPercentage();
    // float cameraServo = BluetoothCar::getInstance()->getCameraServoVerticalPercentage();
    // bool steerPosition = BluetoothCar::getInstance()->getSteerPosition();
    bool urlReceived = BluetoothCar::getInstance()->getUrlReceived();
    // bool motorPosition = BluetoothCar::getInstance()->getMotorPosition();
    // bool cameraHorizontal = BluetoothCar::getInstance()->getCameraHorizontalPosition();
    // bool cameraVertical = BluetoothCar::getInstance()->getCameraVerticalPosition();
    // float cameraHServo = BluetoothCar::getInstance()->getCameraServoHorizontalPercentage();

    // static bool flag = false;
    if(!urlReceived)
    {
        BluetoothCar::getInstance()->sendUrlToCar("http://192.168.43.32");
        // u0_dbg_printf("tick\r\n");
        // flag = true;
    }

    // static int count = 0;
    // char buffer[10] = {'\0'};
    // BluetoothCar::getInstance()->sendDistanceToCar(itoa(count, buffer, 10));
    // count++;

    // u0_dbg_printf("%f %f %f %d %d %d %d %f\r\n", motor, carServo, cameraServo, steerPosition, motorPosition, cameraHorizontal, cameraVertical, cameraHServo);
    // vTaskDelay(250);

    vTaskDelay(250U);

    return true;
}
