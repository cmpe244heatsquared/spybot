#include "driver.hpp"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "bluetooth.hpp"
#include "car.hpp"
#include "io.hpp"
#include "motor.hpp"
#include "printf_lib.h"
#include "proximity.hpp"
#include "servo.hpp"

#define MIN(x, y) ((x < y) ? x : y)
#define MAX(x, y) ((x > y) ? x : y)

#define FRONT_DISTANCE_THRESH 300U  // mm

typedef struct {
    Car *car;
    BluetoothCar *bluetooth_handle;
} Driver_S;

static Driver_S driver;


void DRIVER_init(void)
{
    Motor *new_motor = new Motor(PWM::pwm1, 100U);
    Servo *new_servo = new Servo(PWM::pwm2, 100U);
    driver.car = new Car(new_motor, new_servo);
    driver.bluetooth_handle = BluetoothCar::getInstance();
}


void DRIVER_task(void *params)
{
    uint16_t counter = 0U;
    driver.car->initialize_esc();

    while (true) {
        volatile float motor_percent = driver.bluetooth_handle->getMotorPercentage();
        volatile float servo_percent = driver.bluetooth_handle->getCarServoPercentage();
        volatile bool motor_position = driver.bluetooth_handle->getMotorPosition();
        volatile bool steer_position = driver.bluetooth_handle->getSteerPosition();

        if (motor_position) {
            driver.car->move_backward(motor_percent);
        }
        else {
            if (PROXIMITY_distance(PROXIMITY_FRONT) < FRONT_DISTANCE_THRESH) {
                motor_percent = MIN(motor_percent, 0.0F);
            }
            driver.car->move_forward(motor_percent);
        }

        if (steer_position) {
            driver.car->steer_right(servo_percent);
        }
        else {
            driver.car->steer_left(servo_percent);
        }

        if (counter++ > 500U) {
            counter = 0U;
            char buffer[10] = {'\0'};
            driver.bluetooth_handle->sendDistanceToCar(itoa((int)PROXIMITY_distance(PROXIMITY_FRONT), buffer, 10));
        }

        vTaskDelay(1U);
    }
}
