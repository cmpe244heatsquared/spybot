#include "motor.hpp"


Motor::Motor(PWM::pwmType pwm, unsigned int frequency_hz)
{
    this->pwm = new PWM(pwm, frequency_hz);
}


bool Motor::set(float percent)
{
    return this->pwm->set(percent);
}
