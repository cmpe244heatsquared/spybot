/*
 * @author jtran
 * @version 1.0.0
 */

#ifndef SERVO_HPP
#define SERVO_HPP

#include "motor.hpp"

class Servo : public Motor
{
public:
    Servo(PWM::pwmType pwm, unsigned int frequency_hz);
};


#endif  // SERVO_HPP
