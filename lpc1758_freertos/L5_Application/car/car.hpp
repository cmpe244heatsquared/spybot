/*
 * @author jtran
 * @version 1.0.0
 */

#ifndef CAR_HPP
#define CAR_HPP

#include <stdbool.h>
#include <stdint.h>

#include "motor.hpp"
#include "servo.hpp"


class Car
{
public:
    Car(Motor *motor, Servo *servo);

    void initialize_esc(void);

    bool move_forward(float percent);
    bool move_backward(float percent);
    bool steer_right(float percent);
    bool steer_left(float percent);

    bool stop(void);
    bool steer_straight(void);

private:
    Motor *motor;
    Servo *servo;
};


#endif  // CAR_HPP
