#ifndef DRIVER_HPP
#define DRIVER_HPP


void DRIVER_init(void);
void DRIVER_task(void *params);


#endif  // DRIVER_HPP
