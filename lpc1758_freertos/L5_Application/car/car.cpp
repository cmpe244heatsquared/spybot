#include "car.hpp"

#include "FreeRTOS.h"
#include "task.h"

#define MOTOR_FORWARD_MAX_PWM_PERCENT 18.5F
#define MOTOR_BACKWARD_MAX_PWM_PERCENT 13.5F
#define SERVO_RIGHT_MAX_PWM_PERCENT 10.5F
#define SERVO_LEFT_MAX_PWM_PERCENT 19.5F
#define MOTOR_MIN_PWM_PERCENT 15.0F
#define SERVO_MIN_PWM_PERCENT 15.0F

#define TO_FACTOR(percent) (percent / 100.0F)  // Change domain from [0,100] to [0,1]
#define NORMALIZE_PWM_PERCENTAGE(max, min, percent)  (((max - min) * TO_FACTOR(percent)) + min) // Change domain from [0,100] to [min,max]


Car::Car(Motor *motor, Servo *servo)
{
    this->motor = motor;
    this->servo = servo;

    this->stop();
    this->steer_straight();
}


void Car::initialize_esc(void)
{
    for (unsigned int count=0U; count<3U; count++) {
        this->stop();
        this->steer_straight();
        vTaskDelay(300U);
    }
}


bool Car::move_forward(float percent)
{
    this->motor->set(NORMALIZE_PWM_PERCENTAGE(MOTOR_FORWARD_MAX_PWM_PERCENT, MOTOR_MIN_PWM_PERCENT, percent));
}


bool Car::move_backward(float percent)
{
    this->motor->set(NORMALIZE_PWM_PERCENTAGE(MOTOR_BACKWARD_MAX_PWM_PERCENT, MOTOR_MIN_PWM_PERCENT, percent));

}


bool Car::steer_right(float percent)
{

    this->servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_RIGHT_MAX_PWM_PERCENT, SERVO_MIN_PWM_PERCENT, percent));
}


bool Car::steer_left(float percent)
{
    this->servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_LEFT_MAX_PWM_PERCENT, SERVO_MIN_PWM_PERCENT, percent));
}


bool Car::stop(void)
{
    this->motor->set(MOTOR_MIN_PWM_PERCENT);
}


bool Car::steer_straight(void)
{ 
    this->servo->set(SERVO_MIN_PWM_PERCENT);
}
