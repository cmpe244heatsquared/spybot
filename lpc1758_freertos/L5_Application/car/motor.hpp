/*
 * @author jtran
 * @version 1.0.0
 */

#ifndef MOTOR_HPP
#define MOTOR_HPP

#include <stdbool.h>
#include <stdint.h>

#include "lpc_pwm.hpp"


class Motor
{
public:
    Motor(PWM::pwmType pwm, unsigned int frequency_hz);
    bool set(float percent);

private:
    PWM *pwm;
};


#endif  // MOTOR_HPP
