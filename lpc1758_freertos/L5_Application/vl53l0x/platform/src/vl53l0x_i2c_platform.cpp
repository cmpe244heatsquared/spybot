#include "vl53l0x_i2c_platform.h"

#include "FreeRTOS.h"
#include "task.h"

#include "i2c2.hpp"


int32_t VL53L0X_comms_initialise(uint16_t comms_speed_khz)
{
    I2C2::getInstance().init((unsigned int)comms_speed_khz);
    return 0;
}


int32_t VL53L0X_comms_close(void)
{
    return 1;
}


int32_t VL53L0X_cycle_power(void)
{
    return 0;
}


int32_t VL53L0X_write_multi(uint8_t address, uint8_t index, uint8_t *pdata, int32_t count)
{
    int32_t error = (int32_t)!I2C2::getInstance().writeRegisters(address, index, pdata, count);
    return error;
}


int32_t VL53L0X_read_multi(uint8_t address, uint8_t index, uint8_t *pdata, int32_t count)
{
    int32_t error = (int32_t)!I2C2::getInstance().readRegisters(address, index, pdata, count);
    return error; 
}


int32_t VL53L0X_write_byte(uint8_t address, uint8_t index, uint8_t data)
{
    int32_t error = VL53L0X_write_multi(address, index, &data, 1);
    return error;
}


int32_t VL53L0X_write_word(uint8_t address, uint8_t index, uint16_t data)
{
    uint8_t buffer[2U] = {0U};
    buffer[0U] = (uint8_t)(data >> 8U);
    buffer[1U] = (uint8_t)(data & 0x00FF);
    int32_t error = VL53L0X_write_multi(address, index, buffer, 2);
    return error;
}


int32_t VL53L0X_write_dword(uint8_t address, uint8_t index, uint32_t data)
{
    uint8_t buffer[4U] = {0U};
    buffer[0U] = (uint8_t)(data >> 24U);
    buffer[1U] = (uint8_t)((data & 0x00FF0000) >> 16U);
    buffer[2U] = (uint8_t)((data & 0x0000FF00) >> 8U);
    buffer[3U] = (uint8_t)(data & 0x000000FF);
    int32_t error = VL53L0X_write_multi(address, index, buffer, 4);
    return error;
}


int32_t VL53L0X_read_byte(uint8_t address, uint8_t index, uint8_t *pdata)
{
    int32_t error = (int32_t)VL53L0X_read_multi(address, index, pdata, 1);
    return error;
}


int32_t VL53L0X_read_word(uint8_t address, uint8_t index, uint16_t *pdata)
{
    uint8_t buffer[2U] = {0U};
    int32_t error = (int32_t)VL53L0X_read_multi(address, index, buffer, 2);
    *pdata = ((uint16_t)buffer[0U] << 8U) + (uint16_t)buffer[1U];
    return error;
}


int32_t VL53L0X_read_dword(uint8_t address, uint8_t index, uint32_t *pdata)
{
    uint8_t buffer[4U] = {0U};
    int32_t error = (int32_t)VL53L0X_read_multi(address, index, buffer, 4);
    *pdata = ((uint32_t)buffer[0U] << 24U) + ((uint32_t)buffer[1U] << 16U) + ((uint32_t)buffer[2U] << 8U) + (uint32_t)buffer[3U];
    return error;
}


int32_t VL53L0X_platform_wait_us(int32_t wait_us)
{
    return 0;
}


int32_t VL53L0X_wait_ms(int32_t wait_ms)
{
    return 0;
}


int32_t VL53L0X_set_gpio(uint8_t level)
{
    return 0;
}


int32_t VL53L0X_get_gpio(uint8_t *plevel)
{
    return 0;
}


int32_t VL53L0X_release_gpio(void)
{
    return 0;
}


int32_t VL53L0X_get_timer_frequency(int32_t *ptimer_freq_hz)
{
    return 0;
}


int32_t VL53L0X_get_timer_value(int32_t *ptimer_count)
{
    return 0;
}
