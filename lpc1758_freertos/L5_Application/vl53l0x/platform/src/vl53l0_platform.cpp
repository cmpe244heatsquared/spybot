#include "vl53l0x_platform.h"

#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"


VL53L0X_Error VL53L0X_LockSequenceAccess(VL53L0X_DEV Dev)
{
    return VL53L0X_ERROR_NONE;
}


VL53L0X_Error VL53L0X_UnlockSequenceAccess(VL53L0X_DEV Dev)
{
    return VL53L0X_ERROR_NONE;
}


VL53L0X_Error VL53L0X_WriteMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, uint32_t count)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_write_multi(Dev->I2cDevAddr, index, pdata, count);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_ReadMulti(VL53L0X_DEV Dev, uint8_t index, uint8_t *pdata, uint32_t count)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_read_multi(Dev->I2cDevAddr, index, pdata, count);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_WrByte(VL53L0X_DEV Dev, uint8_t index, uint8_t data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_write_byte(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_WrWord(VL53L0X_DEV Dev, uint8_t index, uint16_t data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_write_word(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_WrDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_write_dword(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_UpdateByte(VL53L0X_DEV Dev, uint8_t index, uint8_t AndData, uint8_t OrData)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = 0;
    uint8_t data;

    err = VL53L0X_read_byte(Dev->I2cDevAddr, index, &data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }

    if (error == VL53L0X_ERROR_NONE) {
        data = (data & AndData) | OrData;
        err = VL53L0X_write_byte(Dev->I2cDevAddr, index, data);
        if (err != 0) {
            error = VL53L0X_ERROR_CONTROL_INTERFACE;
        }
    }
    return error;
}


VL53L0X_Error VL53L0X_RdByte(VL53L0X_DEV Dev, uint8_t index, uint8_t *data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_read_byte(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_RdWord(VL53L0X_DEV Dev, uint8_t index, uint16_t *data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_read_word(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_RdDWord(VL53L0X_DEV Dev, uint8_t index, uint32_t *data)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    int32_t err = VL53L0X_read_dword(Dev->I2cDevAddr, index, data);
    if (err != 0) {
        error = VL53L0X_ERROR_CONTROL_INTERFACE;
    }
    return error;
}


VL53L0X_Error VL53L0X_PollingDelay(VL53L0X_DEV Dev)
{
    VL53L0X_Error error = VL53L0X_ERROR_NONE;
    vTaskDelay(5U);
    return error;
}
