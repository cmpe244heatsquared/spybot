#include "camera_mount.hpp"
#include "printf_lib.h"

#define SERVO_MAX_PWM_PERCENT 20.0F
#define SERVO_MID_PWM_PERCENT 10.0F
#define SERVO_MIN_PWM_PERCENT 0.0F

#define TO_FACTOR(percent) (percent / 100.0F)  // Change domain from [0,100] to [0,1]
#define NORMALIZE_PWM_PERCENTAGE(max, min, percent)  (((max - min) * TO_FACTOR(percent)) + min) // Change domain from [0,100] to [min,max]


CameraMount::CameraMount(Servo *tilt_servo, Servo *pan_servo)
{
    this->tilt_servo = tilt_servo;
    this->pan_servo = pan_servo;
    this->tilt_up(0.0F);
    this->pan_left(0.0F);
}


bool CameraMount::tilt_up(float percent)
{   
    this->tilt_servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_MAX_PWM_PERCENT, SERVO_MID_PWM_PERCENT, percent));
}


bool CameraMount::tilt_down(float percent)
{
    this->tilt_servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_MIN_PWM_PERCENT, SERVO_MID_PWM_PERCENT, percent));
}


bool CameraMount::pan_left(float percent)
{
    this->pan_servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_MAX_PWM_PERCENT, SERVO_MID_PWM_PERCENT, percent));
}


bool CameraMount::pan_right(float percent)
{
    this->pan_servo->set(NORMALIZE_PWM_PERCENTAGE(SERVO_MIN_PWM_PERCENT, SERVO_MID_PWM_PERCENT, percent));
}
