#ifndef CAMERA_MOUNT_HPP
#define CAMERA_MOUNT_HPP

#include <stdbool.h>

#include "servo.hpp"


class CameraMount
{
public:
    CameraMount(Servo *tilt_servo, Servo *pan_servo);

    bool tilt_up(float percent);
    bool tilt_down(float percent);
    bool pan_left(float percent);
    bool pan_right(float percent);

private:
    Servo *tilt_servo;
    Servo *pan_servo;
};


#endif  // CAMERA_MOUNT_HPP
