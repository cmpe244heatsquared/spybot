#ifndef CAMERA_MOUNT_CONTROLLER_HPP
#define CAMERA_MOUNT_CONTROLLER_HPP


void CMC_init(void);
void CMC_task(void *params);


#endif  // CAMERA_MOUNT_CONTROLLER_HPP
