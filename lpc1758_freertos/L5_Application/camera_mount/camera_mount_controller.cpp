#include "camera_mount_controller.hpp"

#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"

#include "bluetooth.hpp"
#include "camera_mount.hpp"
#include "lpc_pwm.hpp"
#include "servo.hpp"

typedef struct {
    CameraMount *camera_mount;
    BluetoothCar *bluetooth_car;
} CMCVars_S;

static CMCVars_S cmc_vars;

void CMC_init(void)
{
    Servo *new_tilt_servo = new Servo(PWM::pwm3, 500U);
    Servo *new_pan_servo = new Servo(PWM::pwm4, 500U);
    cmc_vars.camera_mount = new CameraMount(new_tilt_servo, new_pan_servo);
    cmc_vars.bluetooth_car = BluetoothCar::getInstance();
}


void CMC_task(void *params)
{
    while (true) {
        volatile float tilt_percent = cmc_vars.bluetooth_car->getCameraServoVerticalPercentage();
        volatile float pan_percent = cmc_vars.bluetooth_car->getCameraServoHorizontalPercentage();
        volatile bool tilt_position = cmc_vars.bluetooth_car->getCameraVerticalPosition();
        volatile bool pan_position = cmc_vars.bluetooth_car->getCameraHorizontalPosition();

        if (tilt_position) {
            cmc_vars.camera_mount->tilt_up(tilt_percent);
        }
        else {
            cmc_vars.camera_mount->tilt_down(tilt_percent);
        }

        if (pan_position) {
            cmc_vars.camera_mount->pan_right(pan_percent);
        }
        else {
            cmc_vars.camera_mount->pan_left(pan_percent);
        }

        vTaskDelay(500U);
    }
}
