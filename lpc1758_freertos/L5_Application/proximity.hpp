#ifndef PROXIMITY_HPP
#define PROXIMITY_HPP

#include <stdint.h>

typedef enum {
    PROXIMITY_FRONT=0,
    PROXIMITY_LAST_INDEX,
} PROXIMITY_Sensor_E;


void PROXIMITY_init(void);
void PROXIMITY_task(void *params);
uint16_t PROXIMITY_distance(PROXIMITY_Sensor_E index);
bool PROXIMITY_is_error(void);
bool PROXIMITY_is_initialized(void);


#endif  // PROXIMITY_HPP
